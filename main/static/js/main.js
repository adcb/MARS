Webcam.set({
  width: 320,
  height: 240,
  image_format: "jpeg",
  jpeg_quality: 90,
});
Webcam.attach("#imageCapture");

document.querySelector("#test").addEventListener("click", function () {
  getExpression();
});

const getExpression = () => {
  Webcam.snap((image_uri) => {
    console.log(image_uri);
    fetch("/expression", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ image_uri: image_uri }),
    })
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        mood = res.mood;
        mood = mood.charAt(0).toUpperCase() + mood.slice(1);
        document.querySelector("#status").innerHTML = `Current Mood: ${mood}`;
        var lang = $("#mylang option:selected").val();
        var opt = $("#myopt option:selected").val();

        // Music
        if (opt == "music") {
          $("#next-movie").hide();
          $("#prev-movie").hide();
          if (mood == "Sad") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgba(14, 9, 121, 1) 69%, rgba(0, 189, 255, 1) 100%)"
            );
            if (lang == "hin") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/6SqOfQySOoUHFiYJsnHGPE?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "tel") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/5EvTbekEgr7McgAWBOoEPY?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "eng") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/3RyEta1bqQ6f6mNy6QjvoQ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            }
          } else if (mood == "Happy") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgba(188, 203, 7, 1) 0%, rgba(219, 203, 88, 1) 100%)"
            );
            if (lang == "hin") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/1tTXdi6Bp04Pgmam9bSN7W?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "tel") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/07aRZdDJN3DyLdNJuh2dbZ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "eng") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/6ynV7pfJ4ywQjTxTWUJ1qf?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            }
          } else if (mood == "Angry") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgb(255, 0, 0) , rgb(255, 0, 76))"
            );
            if (lang == "tel") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/5hwQepZhlnOVzngxKwWP8K?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "eng") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/6NLZ1cNGEfQrwpLGTv0twj?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "hin") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/6meqgtbrZ42UWg4fkT9Xbd?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            }
          } else if (mood == "Calm") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgba(137, 170, 75, 1) 0%, rgba(77, 138, 9, 1) 100%)"
            );
            if (lang == "hin") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/0dsl5hbFdVT7scb7Vakkwa?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "tel") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/4SuwwpSOMmNRhlCCPdG19i?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            } else if (lang == "eng") {
              $(
                '<iframe src="https://open.spotify.com/embed/playlist/0wt5WcUHchD1MXPVX4jDsU?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");
            }
          }
        }
        // Movies
        else if (opt == "mov") {
          $("#prev-movie").show();
          $("#next-movie").show();

          // Sad
          if (mood == "Sad") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgba(14, 9, 121, 1) 69%, rgba(0, 189, 255, 1) 100%)"
            );
            // Sad Hindi
            if (lang == "hin") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              sadhin = [
                "tt0164538",
                "tt0238936",
                "tt1849718",
                "tt2980794",
                "tt4635372",
                "tt1166100",
                "tt0405508",
                "tt2203308",
                "tt1839596",
                "tt8902990",
              ];
              sadhin_ctr = 0;
              src += sadhin[sadhin_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                sadhin_ctr--;
              });
              $("#next-movie").click(function () {
                sadhin_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += sadhin[sadhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += sadhin[sadhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Sad Telugu
            else if (lang == "tel") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              sadtel = [
                "tt8737614",
                "tt2258337",
                "tt7856166",
                "https://dood.watch/e/xug55pj4rkwz",
                "https://dood.watch/e/0whl724dlq0p",
                "https://dood.watch/e/ygj1s29ltvxt",
                "https://dood.watch/e/amliq39aivbo",
                "https://dood.watch/e/9wthys37kyov",
                "https://dood.watch/e/98u95ykmpnv7",
                "https://dood.watch/e/9u9r8kzjsu1l",
              ];
              sadtel_ctr = 0;
              src += sadtel[sadtel_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                sadtel_ctr--;
              });
              $("#next-movie").click(function () {
                sadtel_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (sadtel_ctr > 2) {
                    src = "";
                  }
                  src += sadtel[sadtel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (sadtel_ctr > 2) {
                    src = "";
                  }
                  src += sadtel[sadtel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Sad English
            else if (lang == "eng") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              sadeng = [
                "tt0108052",
                "tt0084707",
                "tt1078588",
                "tt2582846",
                "tt0120338",
                "tt0109830",
                "tt0332280",
                "tt2674426",
                "tt8613070",
                "tt0431308",
                "tt1517451",
              ];
              sadeng_ctr = 0;
              src += sadeng[sadeng_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                sadeng_ctr--;
              });
              $("#next-movie").click(function () {
                sadeng_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += sadeng[sadeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += sadeng[sadeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
          }
          // Happy
          else if (mood == "Happy") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgba(188, 203, 7, 1) 0%, rgba(219, 203, 88, 1) 100%)"
            );
            // Happy Hindi
            if (lang == "hin") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              happyhin = [
                "tt3322420",
                "tt1093370",
                "tt1187043",
                "tt1562872",
                "tt1694542",
                "tt1610452",
                "tt1324059",
                "tt4110568",
                "tt2436516",
                "tt5764096",
              ];
              happyhin_ctr = 0;
              src += happyhin[happyhin_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                happyhin_ctr--;
              });
              $("#next-movie").click(function () {
                happyhin_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += happyhin[happyhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += happyhin[happyhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Happy Telugu
            else if (lang == "tel") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              happytel = [
                "tt8188610",
                "tt9537292",
                "tt6980546",
                "tt7589670",
                "https://dood.watch/e/oiftse02rwg6",
                "https://dood.watch/e/82ulgcpi3pcd",
                "https://dood.watch/e/mty5lo1qmkb4",
                "https://dood.watch/e/i5k9n5wzy3zq",
                "https://dood.watch/e/ixu0snaemmub",
                "https://dood.watch/e/vqpvfca7qgvr",
              ];
              happytel_ctr = 0;
              src += happytel[happytel_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                happytel_ctr--;
              });
              $("#next-movie").click(function () {
                happytel_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (happytel_ctr > 3) {
                    src = "";
                  }
                  src += happytel[happytel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (happytel_ctr > 3) {
                    src = "";
                  }
                  src += happytel[happytel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Happy English
            else if (lang == "eng") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              happyeng = [
                "tt0264464",
                "tt0114709",
                "tt1010048",
                "tt0362227",
                "tt3783958",
                "tt0112697",
                "tt0457939",
                "tt2503944",
                "tt0314331",
                "tt0795421",
              ];
              happyeng_ctr = 0;
              src += happyeng[happyeng_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                happyeng_ctr--;
              });
              $("#next-movie").click(function () {
                happyeng_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += happyeng[happyeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += happyeng[happyeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
          }
          // Angry
          else if (mood == "Angry") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgb(255, 0, 0) , rgb(255, 0, 76))"
            );
            // Angry Telugu
            if (lang == "tel") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              angtel = [
                "tt11191344",
                "tt8107464",
                "tt7765910",
                "https://dood.watch/e/lowi5kfgpg2x",
                "https://dood.watch/e/i7nahu1klavo",
                "https://dood.watch/e/uo719f454jln",
                "https://dood.watch/e/dl7r2jzocp8c",
                "https://dood.watch/e/d1kidmvz8w5n",
                "https://dood.watch/e/k4t3ar2w15js",
                "https://dood.watch/e/4ymhcnsdzl5o",
              ];
              angtel_ctr = 0;
              src += angtel[angtel_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                angtel_ctr--;
              });
              $("#next-movie").click(function () {
                angtel_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (angtel_ctr > 2) {
                    src = "";
                  }
                  src += angtel[angtel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (angtel_ctr > 2) {
                    src = "";
                  }
                  src += angtel[angtel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Angry English
            else if (lang == "eng") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              angeng = [
                "tt0103064",
                "tt0083944",
                "tt0082971",
                "tt0455362",
                "tt3731562",
                "tt1663202",
                "tt7286456",
                "tt4160708",
                "tt0381061",
                "tt0840361",
              ];
              angeng_ctr = 0;
              src += angeng[angeng_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                angeng_ctr--;
              });
              $("#next-movie").click(function () {
                angeng_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += angeng[angeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += angeng[angeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Angry Hindi
            else if (lang == "hin") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              angryhin = [
                "tt5074352",
                "tt4832640",
                "tt4864932",
                "tt9877170",
                "tt7430722",
                "tt2016894",
                "tt3863552",
                "tt1833673",
                "tt6826438",
                "tt6264938",
              ];
              angryhin_ctr = 0;
              src += angryhin[angryhin_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                angryhin_ctr--;
              });
              $("#next-movie").click(function () {
                angryhin_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += angryhin[angryhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += angryhin[angryhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
          }
          // Calm
          else if (mood == "Calm") {
            $("body").css(
              "background-image",
              "linear-gradient(to bottom, rgba(137, 170, 75, 1) 0%, rgba(77, 138, 9, 1) 100%)"
            );
            // Calm Hindi
            if (lang == "hin") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              calmhin = [
                "tt7919680",
                "tt5946128",
                "tt2350496",
                "tt2181931",
                "tt1934231",
                "tt0367110",
                "tt3767372",
                "tt2372678",
                "tt4900716",
                "tt6588966",
              ];
              calmhin_ctr = 0;
              src += calmhin[calmhin_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                calmhin_ctr--;
              });
              $("#next-movie").click(function () {
                calmhin_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += calmhin[calmhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += calmhin[calmhin_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Calm Telugu
            else if (lang == "tel") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              calmtel = [
                "tt9665402",
                "tt7853292",
                "tt7711342",
                "https://dood.watch/e/rvmzb3ipbzpq",
                "https://dood.watch/e/xymo5gxkt100",
                "https://dood.watch/e/j9pzspa7r51c",
                "https://dood.watch/e/3eclqghok8of",
                "https://dood.watch/e/rssf5h1pnybl",
                "https://dood.watch/e/ecli6fd125xg",
                "https://dood.watch/e/m3oe9t5ipu0h",
              ];
              calmtel_ctr = 0;
              src += calmtel[calmtel_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                calmtel_ctr--;
              });
              $("#next-movie").click(function () {
                calmtel_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (calmtel_ctr > 2) {
                    src = "";
                  }
                  src += calmtel[calmtel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  if (calmtel_ctr > 2) {
                    src = "";
                  }
                  src += calmtel[calmtel_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
            // Calm English
            else if (lang == "eng") {
              var src = "https://www.2embed.ru/embed/imdb/movie?id=";
              calmeng = [
                "tt0454876",
                "tt0112471",
                "tt0381681",
                "tt2209418",
                "tt0129387",
                "tt1632708",
                "tt1142988",
                "tt2194499",
                "tt1028532",
                "tt4481414",
              ];
              calmeng_ctr = 0;
              src += calmeng[calmeng_ctr];
              $(
                '<iframe src="' +
                  src +
                  '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
              ).prependTo(".spot");

              $("#prev-movie").click(function () {
                calmeng_ctr--;
              });
              $("#next-movie").click(function () {
                calmeng_ctr++;
              });

              $(document).ready(function () {
                $("#next-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += calmeng[calmeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });

              $(document).ready(function () {
                $("#prev-movie").click(function () {
                  var src = "https://www.2embed.ru/embed/imdb/movie?id=";
                  src += calmeng[calmeng_ctr];
                  $(
                    '<iframe src="' +
                      src +
                      '" width="100%" height="300" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>'
                  ).prependTo(".spot");
                });
              });
            }
          }
        }
      });
  });
};

setTimeout(() => {
  getExpression();
}, 2000);
