# MARS - Mood Analysis Based Music and Movie Recommendation System

A player that captures the image of the user and provides a player for music and movies based on the captured emotion.

## Setup
1. Clone the repo and create a virtual environment using

   ```python
   python -m venv run
   ```

2. Activate the virtual environment using

   ```python
   .\run\Scripts\activate
   ```

3. Install the requirements using

   ```python
   pip install -r requirements.txt
   ```

4. Start the server using

   ```python
   python manage.py runserver
   ```

5. Open localhost:8000 in the browser to launch the application.

## Usage

1. Select the feature (Music / Movies).
2. Select the language (Telugu / Hindi / English).
3. Click on "Detect New Mood".
4. The embedded player correspondingly shows the content and provides controls.

## Note

This application was last tested successfully with Python 3.10.4.

The dataset used to train the model was [FER - 2013](https://www.kaggle.com/datasets/msambare/fer2013). It has been split into train and test sets separately and is present in the "model" folder. The notebook (model_run.ipynb) that has been used to train is also present in the same folder.

The trained classifier "emotion_classifier.h5" is already present in the "main" folder. You do not need to train the model unless you specifically want to. In that case, you would have to replace the .h5 file with your classifier.
